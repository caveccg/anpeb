"""

    This is a generic BlenderVR processor for use
    with CCG's Vicon system as head tracker.

"""


import blendervr
import datetime
import math
import os
import sys



if blendervr.is_virtual_environment():
    import bge
    from mathutils import Euler
    from blendervr.player import device
    class Processor(blendervr.processor.getProcessor()):
        def __init__(self, parent):
            super(Processor, self).__init__(parent)
            if self.blenderVR.isMaster(): 
                from blendervr.interactor.head_controlled_navigation import HCNav
                self._navigator = HCNav(self, method=None, one_per_user=True)
                self.registerInteractor(self._navigator)
                self.blenderVR.getSceneSynchronizer().getItem(bge.logic).activate(True, True)
            self.last_pos = None
            self.last_mpos = None
            self.read_head_marker_transform()
            self.corrot = Euler((0, 3.1415, 0), 'XYZ').to_matrix()
        
        def start(self):
            """
            BlenderVR Callback, called at BlenderVR start.
            """
            self.logger.debug("## Start my Processor")
            if self.blenderVR.isMaster():
                try:
                    # get access to BlenderVR OSC API
                    self.OSC = self.blenderVR.getPlugin('osc')


                    try: # check if OSC client is available
                        osc_isAvailable = self.OSC.isAvailable()
                        self.logger.debug('OSC Client is available:', osc_isAvailable)
                    except AttributeError: # i.e. plugin self.OSC not instantiated
                        self.logger.warning('OSC plugin not/badly defined in configuration file -> OSC disabled')

                    else:

                        # Define global OSC parameters
                        self.OSC.getGlobal().start(True) # OSC msg: '/global start 1'
                        self.OSC.getGlobal().mute(False) # OSC msg: '/global mute 0'
                        self.OSC.getGlobal().volume('%100') # OSC msg: '/global volume %40'

                        # Print current blendervr users <-> osc users mapping
                        osc_users_dict = self.OSC.getUsersDict()
                        self.logger.debug('Current OSC mapping (OSC user ... is attached to BlenderVR user ...):')
                        for listener_name in osc_users_dict.keys():
                            osc_user = osc_users_dict[listener_name]
                            bvr_user = osc_user.getUser()
                            if bvr_user: self.logger.debug('-', osc_user.getName(), '<->', bvr_user.getName())
                            else:  self.logger.debug('-', osc_user.getName(), '<->', None)

                        # Define OSC users parameters
                        osc_user = self.OSC.getUser('Binaural 1')
                        osc_user.start(True) # OSC msg: '/user 1 start 1'
                        osc_user.mute(False) # OSC msg: '/user 1 mute 0'
                        osc_user.volume('%100') # OSC msg: '/user 1 volume %80'
                        # or equivalently, see .xml configuration
                        bvr_user = self.blenderVR.getUserByName('user A')
                        osc_user = self.OSC.getUser(bvr_user)

                        # Define OSC objects parameters
                        scene = bge.logic.getCurrentScene()
                        kx_object = scene.objects['gascar']
                        osc_object = self.OSC.getObject(kx_object)
                        # because of the previous line, the kx_object 4x4 orientation matrix will
                        # 1) be sent to the OSC client
                        # 2) be updated each time the kx_object moved
                        #osc_object.sound('Z:/AnPeB/Avenida_Sao_Goncalo_Guimaraes/condicao3_2Di35m_gas.wav') # OSC msg: '/object 1 sound HeyPachuco.wav'
                        #osc_object.sound('Z:/AnPeB/tests/Dynamic_Sound_Test/condicao3_2Di35m_gas.wav') # OSC msg: '/object 1 sound HeyPachuco.wav'
                        osc_object.loop(False) # OSC msg: '/object 1 loop 1'
                        osc_object.mute(True) # OSC msg: '/object 1 mute 0'
                        osc_object.volume('%100') # OSC msg: '/object 1 volume %45'
                        osc_object.start(False) # OSC msg: '/object 1 start 1'


                        # Route OSC object sound channel to OSC user (for selective listening)
                        osc_objectUser = self.OSC.getObjectUser(osc_object, osc_user)
                        osc_objectUser.mute(False) # OSC msg: '/objectUser 1 0 mute 0'
                        osc_objectUser.volume('%100') # OSC msg: '/objectUser 1 0 volume %50'


                except:
                    # this try/except using self.logger.log_traceback(False) is the best way
                    # to trace back errors happening on console/master/slaves in BlenderVR.
                    # Without it, some errors won't be printed out in either windows (console's nor master's nor slave's).
                    self.logger.log_traceback(False)
                
        def read_head_marker_transform(self):
            with open("/home/ccg/shared/head-marker-transform.txt") as in_:
                values = in_.read().split()
            rotpos = [float(v) for v in values]
            rot = Euler(rotpos[:3])
            self.head_mtx = rot.to_matrix().to_4x4()
            self.head_mtx.translation = rotpos[3:]
            
        def setTrigger(self, x, y, z, debug=False):
            ''' This method receives the user position
            and sets in the blender environment a Trigger and a ReturnPos property
            inside an object called 'logic core' to trigger trials according to a
            certain threshold
            Three properties store the real user position in the blender env''' 
            scene = bge.logic.getCurrentScene()
            lcore = scene.objects['logic core']
                
            if debug:
                #self.logger.debug('X: ' + str(int(x*100)/100) + ' Y: ' + str(int(y*100)/100) + ' Z: ' + str(int(z*100)/100))
                self.logger.debug('X: ' + str(int(x*100)/100) + ' Y: ' + str(int(y*100)/100))
            lcore['subject_posX'] = x
            lcore['subject_posY'] = y
            lcore['subject_posZ'] = z
            # Calculate the line of passage for the trigger:
            boolTrigger = bool(y>-2.1445*x - 9.1009)
            boolTriggerPass1 = bool(y>-2.1445*x - 4.40)
            boolTriggerPass2 = bool(y>-2.1445*x - 2.0)
            boolTriggerPass3 = bool(y>-2.1445*x + 1.59)
            #boolTrigger = bool(y>-1.449584546*x-2)
            # Calculate the line of passage to define the Return Pos
            boolReturnPos = bool(y<-2.1445*x - 9.5)
            #boolReturnPos = bool(y<-2.1445*x - 0.5)
            #boolReturnPos = bool(y<-1.449584546*x-2.5)
            if debug:
                self.logger.debug('Trigger: ' + str(boolTrigger))
                self.logger.debug('ReturnPos: ' + str(boolReturnPos))
            # Change blender properties:
            if boolTrigger:
                lcore["Trigger"]=True
                
                if boolTriggerPass1:
                    lcore["boolTriggerPass1"]=True
                else:
                    lcore["boolTriggerPass1"]=False
                if boolTriggerPass2:
                    lcore["boolTriggerPass2"]=True
                else:
                    lcore["boolTriggerPass2"]=False
                if boolTriggerPass3:
                    lcore["boolTriggerPass3"]=True
                else:
                    lcore["boolTriggerPass3"]=False
            else:
                lcore["Trigger"]=False
            if boolReturnPos:
                lcore["ReturnPos"]=True
            
        def user_position(self, info):
            mat = info["matrix"]
            mat = mat * self.head_mtx
            x_pos = mat.to_translation()[0]
            y_pos = mat.to_translation()[1]
            z_pos = mat.to_translation()[2]
            self.setTrigger(x_pos, y_pos, z_pos, debug=False)
            r1, r2 = mat[2].copy(), mat[1].copy()
            mat[1], mat[2] = r1, (r2 * -1)
            # Apply 1.2m center to groud, plus diff between vicon origin and eyes center
            #mat[1][3] -= 1.2 + .36
            # Using platforms
            #mat[1][3] -= 1.2 + .44
            # By real ground:
            mat[1][3] -= 1.2 + .26
            #self.logger.debug('Final Z: ' + str(mat[1][3]))
            mat[2][3] -= 2
            #self.logger.debug("Final: ",mat[1][3])
            mat2 = (self.corrot * mat.to_3x3()).to_4x4()
            mat2.translation = mat.translation
            mat = mat2
            info["matrix"] = mat
            super(Processor, self).user_position(info)
            for user in info['users']:
                self._navigator.setHeadLocation(user, info)
                
        def run (self):
            """
            BlenderVR Callback, called every frame.
            """
            # self.logger.debug('######## RUN')
            scene = bge.logic.getCurrentScene()
            lcore = scene.objects['logic core']
            if lcore["auxplaysound"]:
                kx_object = scene.objects['gascar']
                osc_object = self.OSC.getObject(kx_object)
                osc_object.start(False) # OSC msg: '/object 1 start 1'      #
                osc_object.volume('%100') # OSC msg: '/object 1 volume %45' #
                soundName = lcore["soundString"]
                osc_object.sound(soundName)
                osc_object.mute(False) # OSC msg: '/object 1 mute 0'        #
                osc_object.start(True) # OSC msg: '/object 1 start 1'       #
                self.logger.warning('Palying sound...', soundName)
                self.logger.warning('auxplaysound...', lcore["auxplaysound"])
                lcore["auxplaysound"] = False
                self.logger.warning('auxplaysound...', lcore["auxplaysound"])
            return

        def quit(self):
            """
            BlenderVR Callback, called at run stop.
            """
            if self.blenderVR.isMaster():
                try:
                    ## it seems that reset flag is updated but
                    ## that the associated callback (run) is killed before
                    ## it can actually update anything
                    # self.OSC.getGlobal().reset()

                    # this works :)
                    self.OSC.reset() # sends "/global reset" OSC msg
                    self.logger.debug("## Quit my Processor")
                except:
                    self.logger.log_traceback(False)



else: # not VR screen => Console
    class Processor(blendervr.processor.getProcessor()):
        def __init__(self, parent):
            super(Processor, self).__init__(parent)
            from blendervr.interactor.head_controlled_navigation import HCNav
            self._navigator = HCNav(self)
            self.registerInteractor(self._navigator)

