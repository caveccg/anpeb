#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os
import sys
import time
import socket
from itertools import product

sys.path.append("../../..")
sys.path.append("..")
import aserver
import numpy as np
import glob

#Read the folder content:
folder = 'data/sounds/'
sys.path.append(folder)
filenames = glob.glob(folder+'*.wav')


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("0.0.0.0", 10101))
sock.setblocking(0)

#period = 132    # 1.4 ms @ 44100 Hz
period = 441


core = aserver.Core()
core.set_period(period)
core.set_processor()
core.alsa_output(dump_config=True, blocking=False)


gid = core.add_generator(aserver.GeneratorType.WAVE)
cfg = core.new_config("wave")
cfg.config.flags = aserver.WaveFlags("WAVE_INDEX PLAYBACK_COMMAND").value

pre_trial = core.get_wave_index(
    "/usr/lib/libreoffice/share/gallery/sounds/apert.wav"
)

def load_wave(filename):
    return core.get_wave_index(filename)


waves = {
    (str(s)): load_wave(s)
    for s in filenames
}
print waves
print "done"

#cfg.waveIndex = core.add_wave(
#    441, 1,
#    (3.2e4 * np.cos(np.linspace(0, np.pi * 10, 441))).astype(np.int16)
#)
cfg.waveIndex = 0
cfg.command = aserver.PlaybackCommand.PLAY
core.add_source()


print "starting render loop"
core.start_render_loop()

done = False
last_play = 0
while not done:
    try:
        time.sleep(1e-6)
        if time.time() - last_play > 170:  # 2m50s
            cfg.waveIndex = pre_trial
            core.configure_generator(gid, cfg)
            print "anti-noise", time.ctime()
            last_play = time.time()
        try:
            data, addr = sock.recvfrom(1024)
        except:
            continue
        key = data
        print key

        if key in waves:
            cfg.waveIndex = waves[key]
            #time.sleep(0.01)  # sync with video frame
            core.configure_generator(gid, cfg)
            print "playing", key
            last_play = time.time()
        elif key == ("p",):
            cfg.waveIndex = pre_trial
            core.configure_generator(gid, cfg)
            print "playing", key
            last_play = time.time()
        else:
            print "key", key, "not valid"
    except KeyboardInterrupt:
        done = True

print "done"

core.stop_render_loop()
core.shutdown()
sock.close()
