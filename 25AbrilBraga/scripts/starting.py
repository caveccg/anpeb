import time
import bge
import csv
import os
import sys
from random import shuffle
import subprocess



scene = bge.logic.getCurrentScene()
controller = bge.logic.getCurrentController()
obj = controller.owner
eletriccar = scene.objects["eletriccar"]
gascar = scene.objects["gascar"]
brTireElectric = scene.objects["BRTireElectric"]
frTireElectric = scene.objects["FRTireElectric"]
flTireElectric = scene.objects["FLTireElectric"]
blTireElectric = scene.objects["BLTireElectric"]
brTireGas = scene.objects["BRTireGas"]
frTireGas = scene.objects["FRTireGas"]
flTireGas = scene.objects["FLTireGas"]
blTireGas = scene.objects["BLTireGas"]

#Variables
readyset = 0
positions = []
startTrial = 0
t0 = 0.0
count = 0
fileVelocity = ""
size = 0
answer = 0
trialname = ""
responsetimeStart = 0.0
carType = 0
filePositions = []
nextTrialready = 0
nextTrialstart = 0
numberTrial = 0
carVelocity = 0
clock2 = 0.0
carSpeed = 0.0
breakCount = 0

dir_path = os.getcwd()
print("\nPATH: ", dir_path)
#filenamepath = dir_path + '\\data\\'
filenamepath = dir_path + '/data/'
filename = filenamepath + 'inputData2staticNEW.csv'
#print("FileName: ", filename)


def readFile():
	global filename
	
	trials = []
	with open(filename) as csvfile:
		csv_reader = csv.reader(csvfile, delimiter = ',')
		try:
			next(csv_reader)
			for row in csv_reader:
				trials.append(row)
		except csv.Error as e:
			sys.exit('file {}, line {}: {}'.format(filename, csv_reader.line_num, e))

	csvfile.close()
	print("\nTrials: ", trials)
	return trials
	
def randomTrial():
	
	gascar.position.x = 24
	eletriccar.position.x = 35
	
	trials = readFile()
	trials = trials * obj["Repetitions"]
	shuffle(trials)
	
	return trials 

def velocityFile():
	global filenameVelocity
	global trialname
	global positions
	global dir_path
	global filePositions
	global numberTrial
	
	if numberTrial == 0:
	   filePositions = randomTrial()

	if not filePositions:
		print("\n\nList is empty")
		bge.logic.endGame()
	else:
		fileVelocity = filePositions.pop(0)
		trialname = fileVelocity[0]
		print("\nTrialName: ", trialname)

			
		for file in os.listdir(dir_path + "/data"):
			if file == fileVelocity[0]+".csv":
				filenameVelocity = filenamepath + file
				print("\nFileNameVelocity: " , filenameVelocity)
				
				#Read Velocity file:
				positions = []
				with open(filenameVelocity) as csvfile:
					csv_reader = csv.reader(csvfile, delimiter = ';')
					try:
						next(csv_reader)
						for row in csv_reader:
							positions.append(row)
					except csv.Error as e:
						sys.exit('file {}, line {}: {}'.format(filename, csv_reader.line_num, e))   	
		filenameVelocity = ""
		return positions	

def lerp(a, b, time):
 
	return (1 - (time*6)) * float(a) + (time*6) * float(b)  

def playSound():
	global trialname
	
	if obj["playsound"]:
		#import socket
		#sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		#trialNameSound = "data\\sounds\\" + trialname + ".wav"
		trialNameSound = dir_path + "/data/sounds/" + trialname + ".wav"
		#process = subprocess.Popen("D:\\Programas\\VLC\\vlc.exe " + trialNameSound, shell = True)
		process = subprocess.Popen("aplay "+trialNameSound, shell=True)
		print('Playing Sound: '+trialNameSound)
		#sock.sendto(trialNameSound.encode("ascii"), ("172.16.100.7", 10101))

def rotate(velocity, carType):
	#print("VELOCITY: ", velocity)
	if float(velocity) < 35.0 and float(velocity) > 25.0:
		velocity = 30.0 
	elif float(velocity) < 25.0 and float(velocity) > 20.0:
		velocity = 22.5
	elif float(velocity) < 20.0 and float(velocity) > 10.0:
		velocity = 15.0
	elif float(velocity) < 10.0 and float(velocity) > 5.0:
		velocity = 6.5
	elif float(velocity) < 5.0 and float(velocity) > 1.5:
		velocity = 2.5
	elif float(velocity) < 1.5 and float(velocity) > 0.9:
		velocity = 0.95
	elif float(velocity) < 0.9 and float(velocity) > 0.8:
		velocity = 0.85
	elif float(velocity) < 0.8:
		velocity = 0.0 
	if carType == 0:
		brTireGas.applyRotation([0, (float(velocity))/10, 0])
		frTireGas.applyRotation([0, (float(velocity))/10, 0])
		blTireGas.applyRotation([0, (float(velocity))/10, 0])
		flTireGas.applyRotation([0, (float(velocity))/10, 0]) 
	elif carType == 1:
		brTireElectric.applyRotation([0, (float(velocity))/10, 0])
		frTireElectric.applyRotation([0, (float(velocity))/10, 0])
		blTireElectric.applyRotation([0, (float(velocity))/10, 0])
		flTireElectric.applyRotation([0, (float(velocity))/10, 0])  

def saveAnswers(trialName, trialnumber, carPositionX, carPositionY, carPositionZ, carVelocity, keyPressed, carType, sound, responseTime, breakCount):
	# Output destination file:  	
	with open("/home/ccg/shared/Data/output_name.def") as in_:
		output_dest = in_.read().strip()
		with open(output_dest, 'a') as fp:
			#file = dir_path + "\\data\\answers\\Answers.csv"
			print("OUTPUT_DEST: ", output_dest)
			#Open the Summary file 
			fp.write("\n____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________\n")
			# dd/mm/yyyy format
			fp.write(time.strftime("%d/%m/%Y\n"))
			#print 24-hour
			fp.write("Start:"+time.strftime("%H:%M:%S"))
			#Name of the variables:
			fp.write("\n%s%s%s%s%s%s%s%s%s%s%s\n" % ('TrialName;', 'TrialNumber;','CarPositionX;','CarPositionY;','CarPositionZ;','CarVelocity(km/h);',
					'KeyPressed;','CarType;','Sound;','ResponseTime(ms);', 'Pause'))
			#It is introduce the variables for write in the file
			fp.write("%s;" % trialName)
			fp.write("%s;" % str(trialnumber))
			fp.write("%s;" % str(carPositionX))
			fp.write("%s;" % str(carPositionY))
			fp.write("%s;" % str(carPositionZ))
			fp.write("%s;" % str(carVelocity))
			fp.write("%s;" % keyPressed)
			fp.write("%s;" % str(carType))
			fp.write("%s;" % str(sound))
			fp.write("%s;" % ("{0:.3f}".format(responseTime)))
			fp.write("%s" % str(breakCount))
			fp.close()
		in_.close()
	

def ready():
	global readyset
	global positions
	global startTrial
	global t0
	global count
	global size
	global answer
	global trialname
	global responsetimeStart
	global carType
	global nextTrialready
	global nextTrialstart
	global numberTrial
	global dt
	global carVelocity
	global clock2
	global carSpeed
	global breakCount

	
	
	#Keyboard definition:
	spaceKey = obj.sensors["Ready"]

	#Mouse definition:
	mouseMiddle = controller.sensors["MiddleButton"]
	mouseRight = controller.sensors["RightButton"]
	mouseLeft = controller.sensors["LeftButton"]
	breakButton = controller.sensors["Break"]

		
	if (spaceKey.positive and readyset == 0) or nextTrialready == 1:
		nextTrialready = 0
		if numberTrial != 0:
			nextTrialstart = 1
		positions = velocityFile()
		if positions == None:
			readyset = 0
		else:
			readyset = 1
			print("Positions", positions)
			print("\nTrial Name: ", trialname)
			if trialname.find("electric") != -1:
				carType = 1
				print("\ncarType: Eletric")
			
			print("Experiment is ready!")

	if readyset == 1:
		if (mouseMiddle.positive and startTrial == 0) or (nextTrialstart == 1 and numberTrial != 0):
			t0 = time.time()
			responsetimeStart = t0
			playSound()
			numberTrial += 1
			print("numberTrial: ", numberTrial)
			startTrial = 1
			nextTrialstart = 0
			print("\nstartTrial")
			size = len(positions)
			count = 0
		
	if startTrial == 1 and numberTrial != size:
		if count == (size - 2):
			if answer == 0:
				responsetimeEnd = time.time()
				keyPressed = "None"
				
				responseTime = responsetimeEnd - responsetimeStart 
				if carType == 0:
					saveAnswers(trialname, numberTrial, gascar.position.x*-1, gascar.position.y, gascar.position.z, carSpeed, keyPressed, 0, obj["playsound"], responseTime*1000, breakCount)
					print("SaveFile")
				else: 
					saveAnswers(trialname, numberTrial, eletriccar.position.x*-1, eletriccar.position.y, eletriccar.position.z, carSpeed, keyPressed, 1, obj["playsound"], responseTime*1000, breakCount)
					print("SaveFile")
				settime, setposition, carVelocity = positions.pop(0)
				answer   = 1
			if breakCount == 2 or breakCount == 0:
				breakCount = 0
				if obj['timeNewTrial'] > 3.5:
					nextTrialready = 1
					readyset = 0
					startTrial = 0
					carType = 0
					answer = 0
					gascar.position.x = 24
					eletriccar.position.x = 30
					count = 0
			elif breakCount == 1:
				if breakButton.positive:
					#print("Break: ", breakCount)
					breakCount = breakCount + 1
					obj['timeNewTrial'] = 0.0
		else:
			clock = 0.01666
			clock2 = clock2 + clock

			if count < (size - 2):
				if breakButton.positive:
					#print("Breakclock: ", breakCount)
					breakCount = 1
				if count == 0:
					if carType == 0:
						gascar.position.x = float(positions[0][1])
						#gascar.position.z = (gascar.position.x * 10 / 153.2) + 0.54
					else:
						eletriccar.position.x = float(positions[0][1])
						#eletriccar.position.z = (eletriccar.position.x * 10 / 153.2) + 0.54
					carSpeed = float(positions[0][2])
					count = count + 1 
					
				else:
					if clock2 >= (float(positions[1][0]) - float(positions[0][0])):
							
						if carType == 0:
							gascar.position.x = float(positions[1][1])
							#gascar.position.z = (gascar.position.x * 10 / 153.2) + 0.54
						else:
							eletriccar.position.x = float(positions[1][1])
							#eletriccar.position.z = (eletriccar.position.x * 10 / 153.2) + 0.54
						carSpeed = float(positions[1][2])
						settime, setposition, carVelocity = positions.pop(0)
						count = count + 1  
						clock2 = 0
					else:
						pos = lerp(positions[0][1], positions[1][1], clock2)
						if carType == 0:
							gascar.position.x = float(pos)
							#gascar.position.z = (gascar.position.x * 10 / 153.2) + 0.54
						else:
							eletriccar.position.x = float(pos)
							#eletriccar.position.z = (eletriccar.position.x * 10 / 153.2) + 0.54
						carSpeed = lerp(positions[0][2], positions[1][2], clock2)
				rotate(positions[0][2], carType)
				t0 = time.time()
				
			obj['timeNewTrial'] = 0.0

		if mouseRight.positive or mouseLeft.positive:
			if answer == 0:
				responsetimeEnd = time.time()
				if mouseRight.positive:
					keyPressed = "Right"
				elif mouseLeft.positive:
					keyPressed = "Left"

				responseTime = responsetimeEnd - responsetimeStart 
				if carType == 0:
					saveAnswers(trialname, numberTrial, gascar.position.x*-1, gascar.position.y, gascar.position.z, carSpeed, keyPressed, 0, obj["playsound"], responseTime*1000, breakCount)
					print("SaveFile")
				else: 
					saveAnswers(trialname, numberTrial, eletriccar.position.x*-1, eletriccar.position.y, eletriccar.position.z, carSpeed, keyPressed, 1, obj["playsound"], responseTime*1000, breakCount)
					print("SaveFile")
				answer = 1
				

			
	   


	
	
	