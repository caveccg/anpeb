"""

    This is a generic BlenderVR processor for use
    with CCG's Vicon system as head tracker.

"""


import blendervr
import datetime
import math
import os
import sys
import os


if blendervr.is_virtual_environment():
    import bge
    from mathutils import Euler
    from blendervr.player import device
    class Processor(blendervr.processor.getProcessor()):
        def __init__(self, parent):
            super(Processor, self).__init__(parent)
            if self.blenderVR.isMaster(): 
                from blendervr.interactor.head_controlled_navigation import HCNav
                self._navigator = HCNav(self, method=None, one_per_user=True)
                self.registerInteractor(self._navigator)
                self.blenderVR.getSceneSynchronizer().getItem(bge.logic).activate(True, True)
            self.last_pos = None
            self.last_mpos = None
            self.read_head_marker_transform()
            self.corrot = Euler((0, 3.1415, 0), 'XYZ').to_matrix()

        def read_head_marker_transform(self):
            with open("/home/ccg/shared/head-marker-transform.txt") as in_:
                values = in_.read().split()
            rotpos = [float(v) for v in values]
            rot = Euler(rotpos[:3])
            self.head_mtx = rot.to_matrix().to_4x4()
            self.head_mtx.translation = rotpos[3:]
            
        def setTrigger(self, x, y, z, debug=False):
            ''' This method receives the user position
            and sets in the blender environment a Trigger and a ReturnPos property
            inside an object called 'logic core' to trigger trials according to a
            certain threshold
            Three properties store the real user position in the blender env''' 
            scene = bge.logic.getCurrentScene()
            lcore = scene.objects['logic core']
            if debug:
                self.logger.debug('X: ' + str(int(x*100)/100) + ' Y: ' + str(int(y*100)/100) + ' Z: ' + str(int(z*100)/100))
            lcore['subject_posX'] = x
            lcore['subject_posY'] = y
            lcore['subject_posZ'] = z
            # Calculate the line of passage for the trigger:
            boolTrigger = bool(y>-1.45*x - 7.133)
            #boolTrigger = bool(y>-1.449584546*x-2)
            # Calculate the line of passage to define the Return Pos
            boolReturnPos = bool(y<-1.45*x - 7.639)
            #boolReturnPos = bool(y<-1.449584546*x-2.5)
            if debug:
                self.logger.debug('Trigger: ' + str(boolTrigger))
                self.logger.debug('ReturnPos: ' + str(boolReturnPos))
            # Change blender properties:
            if boolTrigger:
                lcore["Trigger"]=True
            else:
                lcore["Trigger"]=False
            if boolReturnPos:
                lcore["ReturnPos"]=True
            
        def user_position(self, info):
            mat = info["matrix"]
            mat = mat * self.head_mtx
            x_pos = mat.to_translation()[0]
            y_pos = mat.to_translation()[1]
            z_pos = mat.to_translation()[2]
            self.setTrigger(x_pos, y_pos, z_pos, debug=False)
            r1, r2 = mat[2].copy(), mat[1].copy()
            mat[1], mat[2] = r1, (r2 * -1)
            # Apply 1.2m center to groud, plus diff between vicon origin and eyes center
            # Using platforms
            mat[1][3] -= 1.2 + .44
            # By real ground:
            #mat[1][3] -= 1.2 + .26
            #self.logger.debug('Final Z: ' + str(mat[1][3]))
            mat[2][3] -= 2
            #self.logger.debug("Final: ",mat[1][3])
            mat2 = (self.corrot * mat.to_3x3()).to_4x4()
            mat2.translation = mat.translation
            mat = mat2
            info["matrix"] = mat
            super(Processor, self).user_position(info)
            for user in info['users']:
                self._navigator.setHeadLocation(user, info)



else: # not VR screen => Console
    class Processor(blendervr.processor.getProcessor()):
        def __init__(self, parent):
            super(Processor, self).__init__(parent)
            from blendervr.interactor.head_controlled_navigation import HCNav
            self._navigator = HCNav(self)
            self.registerInteractor(self._navigator)

