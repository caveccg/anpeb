import time
import bge
import os
scene = bge.logic.getCurrentScene()
controller = bge.logic.getCurrentController()
obj = controller.owner
car = scene.objects["car"]

    

# Bool control to start experiment (data loaded)
if os._set == 1:
    # First read is to ignore, turn on the isi period
    if os._running_isi == 0 and os._loaded == 0:
        os._running_isi = 1
        os._running_trial = 0
    # isi is starting, load a trial
    elif os._running_isi == 1 and os._loaded == 0:
        print('Starting ISI')
        # Write null response if the participant didn' answered, check if it is the first time
        if os._responseGiven == 0 and os._firstPass == 0:
            print('No response for trial ', os._trial_name)
            response = "{trial_name}, None, None, None, None, None, None\n".format(trial_name=os._trial_name)
            os._trial_name = 'None'
            with open(os._output_dest, 'a') as out:
                out.write(response)
        # after recorded answer, lets place response bool False
        os._responseGiven = 0
        # ensure firstPass bool if off
        os._firstPass = 0
        #load the trial
        # Load the trial
        from AnPeB_functions import *
        # trial is loaded when the isi is running (at the beginning):
        os._running_isi = 1
        os._running_trial = 0
        # record the moment the isi started:
        os._setUp_isi = time.time()
        # get one trial from the trials list
        os._trial = os._trials.pop(0)
        # check if it is the last:
        if (os._trial[0] is 'None'):
            obj["End"] = True
            os._end = True
            os._trial_name = 'None'
            os._set == 0
        # define the vars for this trial:
        else:
            os._trial_name = os._trial[0]
            os._vi = os._trial[1]
            os._pi = os._trial[2]
            os._pit = os._trial[3]
            os._pft = os._trial[4]
            os._vf = os._trial[5]
            os._pf = os._trial[6]
            os._model = os._trial[7]
            # define the trajectories (time and position)
            [os._time_trial, os._pos_trial] = get_trajectory(float(os._pi), float(os._pf), float(os._pit), float(os._pft), float(os._vi), float(os._vf), .005, model_type=os._model)
            print('Trial ', os._trial_name, ' loaded!')
            os._loaded = 1
    # isi is running, trial is loaded, lets check the isi timer:
    elif os._running_isi == 1 and os._loaded:
        # place our object away:
        x = car.worldPosition[0]
        y = car.worldPosition[1]
        z = car.worldPosition[2]
        car.worldPosition = 150, y, z
        # check the timer:
        if (time.time()-os._setUp_isi) > os._isi:
            # the isi ended
            print('Ending ISI, starting trial... ')
            os._running_isi = 0
            os._loaded = 0
            obj["Trial"] = True
            obj["ISI"] = False
            # record the moment the isi ended, the trial started:
            os._trial_stime = time.time()
            os._playingSounds = 1
