import numpy as np


def get_LinearBrake_pattern(
                        initial_pos,
                        final_pos,
                        initial_velocity,
                        final_velocity,
                        time_step,
                        s_time
                        ):
    """
    Linear Braking Pattern.

    This function calculates the movement pattern of a braking vehicle.
    It returns two array, the first one with the time sampling and the
    second with the position data for that time sampling
    e.g. to call:
    time_array, position_array = get_LinearBrake_pattern( 20, 10, 10, 5.5, .01,
                                                            3)
    Inputs:
        initial_pos - initial position meters along decreasing traject (float)
        final_pos - final position meters (<initial_position) where the vehicle
                    stopped the brake (float)
        initial_velocity - velocity at the initial_pos in m/s (float)
        final_velocity - velocity at the final_pos in m/s (float)
        time_step - time sampling period in seconds (float)
        s_time - first element of the time array (float)
    Outputs:
        time_array - numpy array with time sampling
        pos_array - numpy array with positions
    Creator: João Lamas - lamas.jp@gmail.com
    """
    # Model the linear velocity - const accel - get acceleration value:
    d1 = np.abs(initial_pos-final_pos)
    a = (final_velocity**2 - initial_velocity**2) / (2*d1)
    # Init return arrays and vars:
    c_pos = initial_pos
    c_time = s_time
    pos_array = []
    time_array = []
    t = 0
    c_pos_prev = initial_pos
    # Get pos and time, step by step
    while True:
        c_pos = initial_pos - initial_velocity*t - 0.5*(a)*t**2
        if c_pos > c_pos_prev:
            break
        if c_pos < final_pos:
            break
        else:
            c_pos_prev = c_pos
        pos_array.append(c_pos)
        time_array.append(c_time)
        t += time_step
        c_time += time_step
    return np.array(time_array), np.array(pos_array)


def get_HighBrake_pattern(
                        initial_pos,
                        final_pos,
                        initial_velocity,
                        final_velocity,
                        time_step,
                        s_time
                        ):
    """
    High Braking Pattern.

    This function calculates the movement pattern of a braking vehicle.
    It returns two array, the first one with the time sampling and the
    second with the position data for that time sampling
    e.g. to call:
    time_array, position_array = get_HighBrake_pattern( 20, 10, 10, 5.5, .01,3)
    Inputs:
        initial_pos - initial position meters along decreasing traject (float)
        final_pos - final position meters (<initial_position) where the vehicle
                    stopped the brake (float)
        initial_velocity - velocity at the initial_pos in m/s (float)
        final_velocity - velocity at the final_pos in m/s (float)
        time_step - time sampling period in seconds (float)
        s_time - first element of the time array (float)
    Outputs:
        time_array - numpy array with time sampling
        pos_array - numpy array with positions
    Creator: João Lamas - lamas.jp@gmail.com
    """
    # get the mean acceleration:
    d1 = np.abs(initial_pos-final_pos)
    am = (final_velocity**2 - initial_velocity**2) / (2*d1)
    # Init return arrays and vars:
    c_pos = initial_pos
    c_time = s_time
    pos_array = []
    time_array = []
    t = 0
    c_pos_prev = initial_pos
    # model starts with a = 0, will increse linearly until the mid of the
    # trajectory and then decrese linearly too
    a = 0
    # Get pos and time, step by step
    while True:
        c_pos = initial_pos - initial_velocity*t - 0.5*(a)*t**2
        # check if we are turning back
        if c_pos > c_pos_prev:
            break
        # Check if the pos reached the endpoint, if not refresh current pos:
        if c_pos < final_pos:
            break
        else:
            c_pos_prev = c_pos
        # Update acceleration value
        if c_pos >= (initial_pos + final_pos) / 2:
            a += time_step * 2 * am
        elif c_pos < (initial_pos + final_pos) / 2:
            a -= time_step * 2 * am
        pos_array.append(c_pos)
        time_array.append(c_time)
        t += time_step
        c_time += time_step
    return np.array(time_array), np.array(pos_array)


def get_ModelBrake_pattern(
                        initial_pos,
                        final_pos,
                        initial_velocity,
                        model_type,
                        time_step,
                        s_time
                        ):
    """
    Brake Pattern from Model.

    This function calculates the movement pattern of a braking vehicle
    according to a selected model.
    It returns two array, the first one with the time sampling and the
    second with the position data for that time sampling
    e.g. to call:
    time_array, position_array = get_HighBrake_pattern( 20, 10, 10, 5.5, .01,3)
    Inputs:
        initial_pos - initial position meters along decreasing traject (float)
        final_pos - final position meters (<initial_position) where the vehicle
                    stopped the brake (float)
        model_type - the selected model
        time_step - time sampling period in seconds (float)
        s_time - first element of the time array (float)
    Outputs:
        time_array - numpy array with time sampling
        pos_array - numpy array with positions
    Creator: João Lamas - lamas.jp@gmail.com
    """
    from numpy import interp
    from math import floor
    # distance:
    travelled_distance = np.abs(final_pos - initial_pos)
    # time array for pulse:
    x_vel = np.linspace(0, 1, num=(1.0/time_step)+1)
    # Select the model
    if model_type == 'BSD030':
        v_pulse_array = 0.976-(0.637*x_vel)-(1.45*x_vel**2)+(1.28*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'GSD030':
        v_pulse_array = 0.983-(0.271*x_vel)-(2.024*x_vel**2)+(1.497*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'ASD030':
        v_pulse_array = 0.98-(0.431*x_vel)-(1.701*x_vel**2)+(1.33*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'BSD3050':
        v_pulse_array = 0.993-(0.58*x_vel)-(0.608*x_vel**2)+(0.678*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'GSD3050':
        v_pulse_array = 0.995-(0.46*x_vel)-(0.418*x_vel**2)+(0.313*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'ASD3050':
        v_pulse_array = 0.995-(0.49*x_vel)-(0.466*x_vel**2)+(0.405*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'BSD5070':
        v_pulse_array = 1.004-(0.563*x_vel)-(0.147*x_vel**2)+(0.349*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'GSD5070':
        v_pulse_array = 0.995-(0.46*x_vel)-(0.418*x_vel**2)+(0.313*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'ASD5070':
        v_pulse_array = 1.002-(0.81*x_vel)+(0.341*x_vel**2)+(0.027*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'BST':
        v_pulse_array = 1.042-(2.236*x_vel)+(1.534*x_vel**2)-(0.315*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'GST':
        v_pulse_array = 1.022-(0.765*x_vel)-(1.654*x_vel**2)+(1.48*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    elif model_type == 'AST':
        v_pulse_array = 1.033-(1.62*x_vel)+(0.199*x_vel**2)+(0.438*x_vel**3)
        displacement_pulse = np.sum(v_pulse_array)*time_step
    else:
        print('Selected model doesnt exist! ')
        time_array = []
        pos_array = []
        return np.array(time_array), np.array(pos_array)
    # Multiply pulse model by initial velocity
    print('Displacement of pulse: ', displacement_pulse)
    vel_array = v_pulse_array * initial_velocity
    # Get new time dimension adjusted - integrate (total displacement), get
    # the multiplication factor for time, stretch/compress time
    new_displacement = np.sum(vel_array)*time_step
    print('Displacement of pulse*initial_velocity: ', new_displacement)
    print('Displacement of wanted movement: ', travelled_distance)
    dispFactor = travelled_distance/new_displacement
    print('Stretch factor - travel time: ', dispFactor)
    t = np.linspace(0, dispFactor, num=len(vel_array))
    print('Resulting TimeStep: ', t[1]-t[0])
    final_displacement = np.sum(vel_array)*(t[1]-t[0])
    print('Final displacement: ', final_displacement)
    # Define new timestamp to time_step
    # interpol = interp1d(t, vel_array)
    dispFactor = floor(dispFactor*100)/100.0
    time_array = np.linspace(
                                0,
                                dispFactor,
                                num=(dispFactor/time_step)+1
                                )
    new_vel = []
    for i in time_array:
        new_vel.append(interp(i, t, vel_array))
    # new_vel = interpol(time_array)
    # After obtainning the velocity for this movement calculate the positions
    # and time arrays
    new_pos = [initial_pos]
    for i, l in zip(new_vel, range(len(new_vel))):
        new_pos.append(new_pos[l] - (i*time_step))
    pos_array = new_pos[:-1]
    time_array = np.linspace(
                                s_time,
                                s_time+dispFactor,
                                num=(dispFactor/time_step)+1
                                )
    return np.array(time_array), np.array(pos_array)


def get_trajectory(
                    initial_pos,
                    final_pos,
                    initial_brake_pos,
                    final_brake_pos,
                    initial_velocity,
                    final_velocity,
                    time_step,
                    time_stopped=3.0,
                    model_type='CNST'
                    ):
    """
    Trajectory with braking at the middle.

    This function calculates the movement pattern of a braking vehicle.
    It returns two array, the first one with the time sampling and the
    second with the position data for that time sampling
    e.g. to call:
    time_array, position_array = get_trajectory(50, 0, 15, 5.5, 20, 10, .01)
    Inputs:
        initial_pos - initial position in meters of vehicle (float)
        final_pos - final position meters (<initial_position) where the vehicle
            trajectory ends (float)
        initial_brake_pos - position meters (<initial_pos) along decreasing
            trajectory where the vehicle starts to brake (float)
        final_brake_pos - final position meters (<initial_position and
            >final_pos) where the vehicle stopped the brake (float)
        initial_velocity - velocity at the initial_pos in m/s (float)
        final_velocity - velocity at the final_brake_pos in m/s (float)
        time_step - time sampling period in seconds (float)
        time_stopped - time the car stays stopped after complete brake in
            seconds, default=3 seconds (float)
    Outputs:
        time_array - numpy array with time sampling
        pos_array - numpy array with positions
    Creator: João Lamas - lamas.jp@gmail.com
    """
    # Get intial pos1_array and t1_array:
    d1 = np.abs(initial_brake_pos-initial_pos)
    t1 = (d1/initial_velocity)
    t1_array = np.linspace(0, t1, num=(t1/time_step)+1)
    t1_array_len = len(t1_array)
    pos1_array = np.linspace(initial_pos, initial_brake_pos, t1_array_len)
    # Get brake period positions
    if model_type == 'CNST':
        t2_array, pos2_array = get_LinearBrake_pattern(
                                                    initial_brake_pos,
                                                    final_brake_pos,
                                                    initial_velocity,
                                                    final_velocity,
                                                    time_step,
                                                    t1_array[-1]
                                                    )
    else:
        t2_array, pos2_array = get_ModelBrake_pattern(
                                                        initial_brake_pos,
                                                        final_brake_pos,
                                                        initial_velocity,
                                                        model_type,
                                                        time_step,
                                                        t1_array[-1]
                                                        )
    # Get final pos3_array and t3_array if not stopped
    if final_velocity > 0:
        final_velocity = np.abs(pos2_array[-2]-pos2_array[-1])/time_step
        d3 = np.abs(final_pos-final_brake_pos)
        t3 = d3/final_velocity
        # Const velocity case, there is no t2_array:
        if len(t2_array)==0:
            t2_array[0]=t1_array[-1]
        t3_array = np.linspace(
                                t2_array[-1],
                                t2_array[-1]+t3,
                                num=(t3/time_step)+1
                                )
        t3_array_len = len(t3_array)
        pos3_array = np.linspace(pos2_array[-1], final_pos, t3_array_len)
    # The case the car stops completly:
    else:
        t3 = time_stopped
        t3_array = np.linspace(
                                t2_array[-1],
                                t2_array[-1]+t3,
                                num=(t3/time_step)+1
                                )
        t3_array_len = len(t3_array)
        pos3_array = np.full((1, t3_array_len), pos2_array[-1])[0]
    # Join the 3 vectors:
    time_array = list(t1_array[:-1])+list(t2_array[:-1])+list(t3_array[:])
    pos_array = list(pos1_array[:-1])+list(pos2_array[:-1])+list(pos3_array[:])
    return time_array, pos_array


def readAnPeB_realData(
                        csvfilename,
                        obj_id,
                        initial_position,
                        reading_time_step=0.01,
                        time_step=0.01,
                        header_obj_string='Id Objeto',
                        header_px_string='P(X)',
                        header_py_string='P(Y)',
                        header_time_string='Tempo Parcial(S)'
                        ):
    """
    Read data from csv file with real vehicle movement data.

    This function reads a csv file, imports the data of a moving vehicle with
    P(X) and/or P(Y) positions along a real trajectory and outputs a time array
    and a position array to be used in blender with the same movement Pattern
    as the real one.
    It returns two array, the first one with the time sampling respecting the
    time_step input, and the second with the position data for that time
    sampling
    e.g. to call:
    time_array, position_array = readAnPeB_realData('Veiculos.csv', 120, 50)
    Inputs:
        csvfilename - string with the filename to be read
        obj_id - ID (number) of the object we want (from the csv file)
        initial_position - initial point of the vehicle
        reading_time_step=0.01 - if not defined in csv, the timestamp the Data
            from the csv file was recorded
        time_step=0.01 - the time sampling we want in out output
        header_px_string='P(X)' - string on the header to identify the px
        header_py_string='P(Y)' - string on the header to identify the py
        header_time_string='Tempo Parcial(S)' - string on the header to
            identify the time of the obj_id movement
    Outputs:
        time_array - numpy array with time sampling
        pos_array - numpy array with positions
    Creator: João Lamas - lamas.jp@gmail.com
    """
    import csv
    from scipy.interpolate import interp1d
    from math import sqrt
    time_array = []
    pos_array = []
    input_time_array = []
    input_px = []
    input_py = []
    # READ the data
    with open(csvfilename, 'r') as csvfile:
        d = csv.reader(csvfile, delimiter=',')
        h = next(d)
        # Filter by object ID:
        try:
            ObjID_index = h.index(header_obj_string)
        except ValueError:
            print('Object not found in this file')
            return time_array, pos_array
        filtered = list(filter(lambda p: str(obj_id) == p[ObjID_index], d))
        # Get the position of the X data
        try:
            px_index = h.index(header_px_string)
        except ValueError:
            print('Position in x-axis not found in this file')
            px_index = None
        # Get the position of the Y data
        try:
            py_index = h.index(header_py_string)
        except ValueError:
            print('Position in y-axis not found in this file')
            py_index = None
        # If there's no data, we can't do miracles!
        if (px_index is None) and (py_index is None):
            return time_array, pos_array
        # Get the position of the Time array data
        try:
            time_index = h.index(header_time_string)
        except ValueError:
            print('Time index not found in this file. Using predefined...')
            time_index = None
        # check if the input is only from the axis Y:
        if (px_index is None) and (py_index is not None):
            for row in filtered:
                input_py.append(float(row[py_index]))
        # check if the input is only from the axis X:
        elif (py_index is None) and (px_index is not None):
            for row in filtered:
                input_px.append(float(row[px_index]))
        # check if the input is from both X and Y axis:
        elif (py_index is not None) and (px_index is not None):
            for row in filtered:
                input_px.append(float(row[px_index]))
                input_py.append(float(row[py_index]))
        else:
            print('Data not found inside csv for this object')
            return time_array, pos_array
        # Check if there's a time array
        # if not lets create one with reading_time_step interval
        if time_index is not None:
            for row in filtered:
                input_time_array.append(float(row[time_index]))
        else:
            print('Not present - time array')
            input_time_array = np.linspace(
                                            0,
                                            (len(filtered)*reading_time_step)-reading_time_step,
                                            len(filtered)
                                            )
    # Get time array:
    time_array = np.arange(
                            0,
                            input_time_array[-1],
                            time_step
                            )
    # TRANSFORM the input arrays into final time and position arrays
    if (py_index is not None) and (px_index is not None):
        px_interp = interp1d(input_time_array, input_px)
        py_interp = interp1d(input_time_array, input_py)
        px = px_interp(time_array)
        py = py_interp(time_array)
        # Get translational motion point by point, get displacement point
        # by point, taking into accound time_step value:
        di_x = np.diff(px)
        di_y = np.diff(py)
        p = np.sqrt((di_x**2)+(di_y**2))
        # Transform again translational motion into one axis displacement
        # insert negative displacements - we want the car approaching
        d = np.insert(-p, 0, initial_position)
        pos_array = np.cumsum(d)
        pos_array = list(pos_array)
    elif (py_index is None) and (px_index is not None):
        px_interp = interp1d(input_time_array, input_px)
        pos_array = px_interp(time_array)
    elif (py_index is not None) and (px_index is None):
        py_interp = interp1d(input_time_array, input_py)
        pos_array = px_interp(time_array)
    else:
        time_array = []
        pos_array = []
    return list(time_array), list(pos_array)
