"""

    This is a generic BlenderVR processor for use
    with CCG's Vicon system as head tracker.

"""


import blendervr
import datetime
import math
import os
import sys
import os


if blendervr.is_virtual_environment():
    import bge
    from mathutils import Euler
    from blendervr.player import device
    class Processor(blendervr.processor.getProcessor()):
        def __init__(self, parent):
            super(Processor, self).__init__(parent)
            if self.blenderVR.isMaster(): 
                from blendervr.interactor.head_controlled_navigation import HCNav
                self._navigator = HCNav(self, method=None, one_per_user=True)
                self.registerInteractor(self._navigator)
                self.blenderVR.getSceneSynchronizer().getItem(bge.logic).activate(True, True)
            self.last_pos = None
            self.last_mpos = None
            self.read_head_marker_transform()
            self.corrot = Euler((0, 3.1415, 0), 'XYZ').to_matrix()

        def read_head_marker_transform(self):
            with open("/home/ccg/shared/head-marker-transform.txt") as in_:
                values = in_.read().split()
            rotpos = [float(v) for v in values]
            rot = Euler(rotpos[:3])
            self.head_mtx = rot.to_matrix().to_4x4()
            self.head_mtx.translation = rotpos[3:]

        def user_position(self, info):
            mat = info["matrix"]
            mat = mat * self.head_mtx
            x_pos = mat.to_translation()[0]
            y_pos = mat.to_translation()[1]
            z_pos = mat.to_translation()[2]
            scene = bge.logic.getCurrentScene()
            lcore = scene.objects['logic core setUp']
            #self.logger.debug('X: ' + str(int(x_pos*100)/100) + 'Y: ' + str(int(y_pos*100)/100) + 'Z: ' + str(int(z_pos*100)/100))
            lcore['subject_posX'] = x_pos
            lcore['subject_posY'] = y_pos
            lcore['subject_posZ'] = z_pos
            r1, r2 = mat[2].copy(), mat[1].copy()
            mat[1], mat[2] = r1, (r2 * -1)
            # Apply 1.2m center to groud, plus diff between vicon origin and eyes center
            mat[1][3] -= 1.2 + .36
            mat[2][3] -= 2
            #self.logger.debug("Final: ",mat[1][3])
            mat2 = (self.corrot * mat.to_3x3()).to_4x4()
            mat2.translation = mat.translation
            mat = mat2
            info["matrix"] = mat
            super(Processor, self).user_position(info)
            for user in info['users']:
                self._navigator.setHeadLocation(user, info)



else: # not VR screen => Console
    class Processor(blendervr.processor.getProcessor()):
        def __init__(self, parent):
            super(Processor, self).__init__(parent)
            from blendervr.interactor.head_controlled_navigation import HCNav
            self._navigator = HCNav(self)
            self.registerInteractor(self._navigator)

