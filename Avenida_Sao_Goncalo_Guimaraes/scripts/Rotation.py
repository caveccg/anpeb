import bge
import os

def rotate():
    cont = bge.logic.getCurrentController()
    owner = cont.owner
    my_rotation = owner['Rotation']
    try:
        my_rotation = float(os._velocity)
    except:
        pass
    owner.applyRotation([0, my_rotation/35.0, 0])
    
rotate()