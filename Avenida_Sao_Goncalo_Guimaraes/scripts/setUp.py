import time
import bge.logic
import os
from random import shuffle
import csv

running_filename_path = os.path.realpath(__file__)
dir_path = '\\'.join(running_filename_path.split('\\')[:])

scene = bge.logic.getCurrentScene()
controller = bge.logic.getCurrentController()
obj = controller.owner

# Read the input data to set trials:
folder = '/data/'
filename = dir_path+folder+'inputData2static.csv'
print(filename)

# Empty array with trials, read input file with trials definition
trials = []
with open(filename) as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    # Overpass the header:
    next(reader)
    for row in reader:
        trials.append(row)


#Create all trials with repetitions in os
if not hasattr(os, '_trials') or not os._trials:
    os._trials = trials
    os._trials = os._trials * obj["Repetitions"]
    #os._trials = os._trials * 5
    shuffle(os._trials)
    os._trials.append((['None']))
    # append empty to begginigng - to be discharged
    ['Empty', '0', '0', '0', '0', '0', '0', '0']+os._trials

# Define isi time:
if not hasattr(os, '_isi') or not os._isi:
    os._isi = obj["ISI"]
    #os._isi = 2.0
    
## Bool vars to be used
if not hasattr(os, '_running_trial') or not os._running_trial:
    os._running_trial = 0
if not hasattr(os, '_running_isi') or not os._running_isi:
    os._running_isi = 0
if not hasattr(os, '_loaded') or not os._loaded:
    os._loaded = 0
if not hasattr(os, '_firstPass') or not os._firstPass:
    os._firstPass = 1
if not hasattr(os, '_responseGiven') or not os._responseGiven:
    os._responseGiven = 0
if not hasattr(os, '_playingSounds') or not os._playingSounds:
    if obj["playSound"] == True:
        os._playingSounds = 1
        print('Playing Sounds...')
        print('Sound server must be set!')
    else:
        os._playingSounds = 0

# Var to store trial time:
if not hasattr(os, '_trial_stime') or not os._trial_stime:
    os._trial_stime = 0
# Var to store current object velocity:
if not hasattr(os, '_velocity') or not os._velocity:
    os._velocity = 0
if not hasattr(os, '_trial_name') or not os._trial_name:
    os._trial_name = ' '
# Output destination file with header:    
if not hasattr(os, '_output_dest') or not os._output_dest:
    with open("/home/ccg/shared/Data/output_name.def") as in_:
        os._output_dest = in_.read().strip()
        header = "trial_name,car_position,car_velocity,time, CamX, CamY, CamZ\n"
        with open(os._output_dest, 'a') as out:
            out.write(header)

#Set boolean value indicating the experiment is ready to start:
if not hasattr(os, '_set') or not os._set:
    os._set = 1
# Print the trials list:
for a in os._trials:
    print(a)
print("Experiment Ready!")
