import bge
import os
import time
cont = bge.logic.getCurrentController()
own = cont.owner

sens1 = cont.sensors['MouseLeft']
sens2 = cont.sensors['MouseRight']
scene = bge.logic.getCurrentScene()

my_pos_x = own['subject_posX']
my_pos_y = own['subject_posY']
my_pos_z = own['subject_posZ']

# Check if button left or right are positive
if sens1.positive or sens2.positive:
    # Get the object position
    scene = bge.logic.getCurrentScene()
    car = scene.objects["car"]
    pos = car.worldPosition[0]
    # Get the the time since the trial begun
    timeResponse = int((time.time()-os._trial_stime)*100)/100
    # Define the response
    response = "{trial_name}, {car_position}, {car_velocity}, {time}, {pos_x}, {pos_y}, {pos_z}\n".format(trial_name=os._trial_name, car_position=int(pos*100)/100, car_velocity=int(os._velocity*100)/100, time=timeResponse, pos_x=int(my_pos_x*100)/100, pos_y=int(my_pos_y*100)/100, pos_z=int(my_pos_z*100)/100)
    print('Response given: ', response)
    # Save to file:
    with open(os._output_dest, 'a') as out:
        out.write(response)
    # Bool that response was given is True:
    os._responseGiven = 1
    