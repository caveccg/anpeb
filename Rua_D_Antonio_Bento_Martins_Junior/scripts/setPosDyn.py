import time
import bge
import os
import bisect
from numpy import abs
controller = bge.logic.getCurrentController()
obj = controller.owner
scene = bge.logic.getCurrentScene()
car = scene.objects["car"]

# Bool control to start experiment (data loaded)
if os._set == 1:
    # First read is to ignore, start timer:
    if os._running_trial == 0:
        os._running_trial = 1
        # record the moment the trial started:
        os._trial_stime = time.time()
        obj['ReturnPos'] = False
        # start sound
        if os._playingSounds == 1:
            import socket
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            trialNameSound = trialW = "data/sounds/{name}.wav".format(name=os._trial_name)
            print('Playing Sound: '+trialNameSound)
            sock.sendto(trialNameSound.encode("ascii"), ("172.16.100.7", 10101))
    elif os._running_trial == 1:
        obj['ReturnPos'] = False
        os._playingSounds = 0
        # check timer since trial started
        trial_time = time.time()-os._trial_stime
        # Define object position, get current velocity and position:
        pos_idx = bisect.bisect_right(os._time_trial, trial_time)
        pos = os._pos_trial[pos_idx-1]
        if pos_idx > 2:
            os._velocity = abs((os._pos_trial[pos_idx-1]-os._pos_trial[pos_idx-2])/(os._time_trial[pos_idx-1]-os._time_trial[pos_idx-2]))
        x = car.worldPosition[0]
        y = car.worldPosition[1]
        z = car.worldPosition[2]
        # set object position
        car.worldPosition = x, pos, z
        # check if it is the last frame:
        if pos_idx == len(os._pos_trial):
            print('Ending trial')
            obj["Trial"] = False
            obj["ISI"] = True
